+ Start using `commander` for getting parameters instead of environment variables
+ Get parameters for changing behaviour.
+ Get config from a `json` file.
+ Stop using `express` and `connect-modrewrite`
